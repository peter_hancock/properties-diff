# properties-diff

Pretty diff Java properties files

## Install

```
npm install -g git+https://bitbucket.org/peter_hancock/properties-diff.git
```

## Usage

```
properties-diff one.properties two.properties
```

#### With `git difftool`

```
git difftool --extcmd='properties-diff "$1" "$2"' ...
```
